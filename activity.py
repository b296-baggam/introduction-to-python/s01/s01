# 2. Create 5 variables and output them in the command prompt in the following format:

name = "Jose"
age = 38
occupation = "Writer"
movie = "One more chance"
rating = 99.6

# - "I am < name (string)> , and I am < age (integer)> years old, I work as a < occupation (string)> , and my rating for < movie (string)> is < rating (decimal)> %"

print(f"I am {name} , and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {rating} %")

# 3. Create 3 variables, num1, num2, and num3
# - Get the product of num1 and num2 (print in the terminal/console)

num1 = 30
num2 = 5
print(num1*num2)

# - Check if num1 is less than num3 (print in the terminal/console)

num3 = 40
print(num1 < num3)

# - Add the value of num3 to num2
num2 += num3
