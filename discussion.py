# [Section] Comments
# comments in python are done using the # symbol

# [Section] Python Syntax
# print "hello world" in Python
print("Hello World")

# [Section] Indentation
# JS and other programming languages use indentation for code readability only, the indentation in Python is very important
# in pyhton, indentation is used to indicate a block of code
# indetation is used instead of {}. we will not be using ; as well

# [Section] Variables
# containers of data
# a variable is decalared by stating the variable name and assigning a value using the equal sign =.
age = 35
print(age)

# [Section] Naming Convention
# variables in python should begin with a letter (A-Z, a-z), dollar sign ($), or an underscore (_)
# in Python, we use snake casing in terms of naming variales( firstword_secondword )
middle_initial = "C"
print(middle_initial)
# python allows assigning of values to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name1)
print(name2)
print(name3)
print(name4)

# [Section] Data Types
# convey what kind of information a variable holds.
# 1. Strings - "" and '' are both accepted as string literals in python
full_name = "John Doe"
secret_code = 'pa$$word'
print(full_name)
print(secret_code)

# 2. Numbers( int, float, complex ) - integers, floats and complex numbers
num_of_days = 365 # whole
pi_approx = 3.1416 # decimals
complex_num = 1 + 5j # j represents imaginary component

# 3. Boolean(bool) - truth values
# in python, boolean values are printed starting with capital letter
isLearning = True
isDifficult = False

# [Section] Using variable
# use variables by simply calling them through their name
print(isLearning)
print(num_of_days)

# [Section] Terminal outputs
# to use a variable and strings, concatenating symbol(+) must be used
print("My name is " + full_name)
# print("The days of a year is " + num_of_days)

# [Section] Typecasting
# there may be times that we want to specify a type on to a variable
# 1 int() - converts the value into an integer
print(int("9876"))
# 2. float() - converts the value into a float
print(float(3))
# 3. str() - converts the value into strings
print("The days of a year is " + str(num_of_days))

# F-strings are used to avoid type error in printing without the use of typecasting; they work similarly to the string literals of JS
# SYNTAX: f"message {variable}"
print(f"Hi! My name is {full_name} and I am {age} years old.")
# miniactivity
# - create a variable named "company" that has the value of your ecommerce website name
# - create 3 variables named product1, product2, product3 and assign values corresponding to your products
# - print their values in this format: "Welcome to <company> some of our featured products are <product1>, <product2>, and <product3>."
# 7:59 pm; solution discussion; kindly send the output in our batch google chat

company = "Zuitt"
product1 = "Laptop"
product2 = "Unit"
product3 = "GPU"

print(f"Welcome to {company}! Some of our featured products are {product1}, {product2}, and {product3}.")

# [Section] Operations
# arithmetic operations in Python
print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4)
print(2 ** 6)

# assignment operators
num1 = 3
print(num1)
num1 += 4
print(num1)
num1 -= 1
print(num1)
num1 *= 3
print(num1)
num1 /= 2
print(num1)
num1 %= 4
print(num1)

# comparison operators
print(1 == 1)
print(1 != 1)
print(1 > 1)
print(1 < 1)
print(1 >= 1)
print(1 <= 1)
print(1 == "1") #equality and inequality operators also compare the data type of the operands

# logical operators
print(True and False)
print(False or True)
print(not False)